/* Copyright (c) 2018-2022 Perinet GmbH
 * All rights reserved
 *
 * This software is dual-licensed: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License version 3 as
 * published by the Free Software Foundation. For the terms of this
 * license, see http://www.fsf.org/licensing/licenses/agpl-3.0.html,
 * or contact us at https://server.io/contact/ when you want license
 * this software under a commercial license.
 */

// Package apiservice_modbus_tcp_bridge implements the modbus tcp
// bridge part of the unified api (https://gitlab.com/perinet/unified-api).
package main

import (
	"encoding/json"
	"flag"
	"log"

	"gitlab.com/perinet/generic/apiservice/dnssd"
	static_api "gitlab.com/perinet/generic/apiservice/staticfiles"
	httpserver "gitlab.com/perinet/generic/lib/httpserver"
	auth "gitlab.com/perinet/generic/lib/httpserver/auth"
	webhelper "gitlab.com/perinet/generic/lib/utils/webhelper"
	security_api "gitlab.com/perinet/periMICA-container/apiservice/security"

	lifecycle_api "gitlab.com/perinet/periMICA-container/apiservice/lifecycle"
	modbus_api "gitlab.com/perinet/periMICA-container/apiservice/modbusbridge"
	node_api "gitlab.com/perinet/periMICA-container/apiservice/node"
	modbusbridge "gitlab.com/perinet/periMICA-container/app/modbusbridge"
)

var (
	nodeInfo       node_api.NodeInfo
	productionInfo node_api.ProductionInfo
	securityConfig security_api.SecurityConfig
	Logger         log.Logger = *log.Default()
	modbusConfig   modbus_api.ModBusTCPBridgeConfig
)

func init() {
	Logger.SetPrefix("Service ModbusTcp2RtuBridge: ")
	Logger.Println("Starting")

	var data []byte
	// update apiservice node with used services
	services := []string{"node", "security", "lifecycle", "dns-sd", "modbustcp2rtubridge"}
	data, _ = json.Marshal(services)
	webhelper.InternalPut(node_api.ServicesSet, data)

	var err error
	data = webhelper.InternalGet(node_api.NodeInfoGet)
	err = json.Unmarshal(data, &nodeInfo)
	if err != nil {
		Logger.Println("Failed to fetch NodeInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(node_api.ProductionInfoGet), &productionInfo)
	if err != nil {
		Logger.Println("Failed to fetch ProductionInfo: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(security_api.Security_Config_Get), &securityConfig)
	if err != nil {
		Logger.Println("Failed to fetch SecurityConfig: ", err.Error())
	}

	err = json.Unmarshal(webhelper.InternalGet(modbus_api.ModBusTCPBridgeConfig_Get), &modbusConfig)
	if err != nil {
		Logger.Println("Failed to fetch PowermeterSdm630Config: ", err.Error())
	}

	// define service names to be advertised
	var service_names = []string{"_https._tcp", "_mbap._tcp"} // FIXME: Add "_mbap-s._tcp" as soon as it is implemented

	for _, service_name := range service_names {
		var port uint16 = 443
		if service_name == "_mbap._tcp" || service_name == "_mbap-s._tcp" {
			port = uint16(modbusConfig.Port)
		}

		// FIXME: do I need to advertise all TxtRecords also with mbap?
		// start advertising via dnssd
		dnssdServiceInfo := dnssd.DNSSDServiceInfo{ServiceName: service_name, Port: port}
		dnssdServiceInfo.TxtRecord = []string{}
		dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "api_version="+nodeInfo.ApiVersion)
		dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "life_state="+nodeInfo.LifeState)
		dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "application_name="+nodeInfo.Config.ApplicationName)
		dnssdServiceInfo.TxtRecord = append(dnssdServiceInfo.TxtRecord, "element_name="+nodeInfo.Config.ElementName)
		data, _ = json.Marshal(dnssdServiceInfo)
		webhelper.InternalVarsPut(dnssd.DNSSDServiceInfoAdvertiseSet, map[string]string{"service_name": dnssdServiceInfo.ServiceName}, data)
	}
}

func main() {
	var webui_path string
	flag.StringVar(&webui_path, "u", "./webui", "Specify path to serve the web ui. Default is ./webui")
	flag.Parse()

	static_api.Set_files_path(webui_path)

	httpserver.AddPaths(static_api.PathsGet())
	httpserver.AddPaths(security_api.PathsGet())
	httpserver.AddPaths(modbus_api.PathsGet())
	httpserver.AddPaths(node_api.PathsGet())
	httpserver.AddPaths(dnssd.PathsGet())
	httpserver.AddPaths(lifecycle_api.PathsGet())

	auth.SetAuthMethod(securityConfig.ClientAuthMethod)

	httpserver.SetCertificates(httpserver.Certificates{
		CaCert:      webhelper.InternalVarsGet(security_api.Security_TrustAnchor_Get, map[string]string{"trust_anchor": "root_ca_cert"}),
		HostCert:    webhelper.InternalVarsGet(security_api.Security_Certificate_Get, map[string]string{"certificate": "host_cert"}),
		HostCertKey: webhelper.InternalVarsGet(security_api.Security_Certificate_Key_Get, map[string]string{"certificate": "host_cert"}),
	})

	modbusbridge.Start()
	httpserver.ListenAndServe("[::]:443")
}
