# modbusTcp2Rtu

![Overview](webui/static/images/overview.png "Overview")

The Modbus tcp to rtu container provides a modbus tcp to rtu bridge. To connect
a modbus RS485 an USB to RS485 converter needs to be attached to one of the USB
ports of periMICA. Additionally the name of the USB device, the used baud rate
and the TCP port to serve the bridge can be configured.
